declare module 'posts' {
	export interface Post {
		userId: number;
		id: number;
		title: string;
		body: string;
	}
}
