import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import DetailPost from './pages/DetailPost';
import Home from './pages/Home';

const router = createBrowserRouter([
	{
		path: '/',
		element: <Home />,
	},
	{ path: '/:id', element: <DetailPost /> },
]);

function App() {
	return <RouterProvider router={router} />;
}

export default App;
