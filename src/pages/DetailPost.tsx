import { Link, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Post } from 'posts';

const DetailPost: React.FunctionComponent = () => {
	let { id } = useParams();

	const [post, setPost] = useState<Post | null>(null);

	useEffect(() => {
		(async () => {
			const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
			const data = await response.json();
			setPost(data);
		})();
	}, [id]);

	return (
		<main>
			<section className="layout mt-8">
				<Link to={'/'} className="text-sky-500 mb-3 hover:underline hover:decoration-dotted">
					Back to home
				</Link>
				<img src="https://source.unsplash.com/random" className="w-full mt-4 rounded-md h-60 object-cover object-center" alt="Post" />
				<h3 className="text-xl font-semibold mt-6">{post?.title}</h3>
				<p className="mt-6">{post?.body}</p>
			</section>
		</main>
	);
};

export default DetailPost;
