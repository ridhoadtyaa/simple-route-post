import { Post } from 'posts';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PostCard from '../components/PostCard';

const Home: React.FunctionComponent = () => {
	const [posts, setPosts] = useState<Post[] | null>(null);

	useEffect(() => {
		(async () => {
			const response = await fetch('https://jsonplaceholder.typicode.com/posts');
			const data = await response.json();
			setPosts(data);
		})();
	}, []);

	return (
		<>
			<header>
				<nav className="layout my-4">
					<h2 className="text-3xl text-center font-semibold text-sky-400">Posts</h2>
				</nav>
			</header>

			<main>
				<section className="layout mt-8">
					<div className="grid grid-cols-1 md:grid-cols-2 gap-4">
						{posts?.map(post => (
							<PostCard post={post} key={post.id} />
						))}
					</div>
				</section>
			</main>
		</>
	);
};

export default Home;
